var RobotsGame = RobotsGame || {};


RobotsGame.init = function(){

	// Setup rendering engine
	this.renderer = new PIXI.autoDetectRenderer(document.body.scrollWidth, document.body.scrollHeight);
	document.body.appendChild(this.renderer.view);
	this.loader = new PIXI.loaders.Loader();
	this.stage = new PIXI.ParticleContainer();

	// Load Sprites
	this.loader.add('tree', 'img/tree.png');
	this.loader.add('stone', 'img/stone.png');
	this.loader.add('copper', 'img/copper.png');

	// Finish up
	this.loader.once('complete', this.onComplete);
	this.loader.load();

};

RobotsGame.onComplete = function(loader, resources){

	console.info('Assets Loaded');
	r = RobotsGame;

	r.actor = new RobotsGame.Actor( new PIXI.Sprite(resources.tree.texture), 100, 100 );
	r.stage.addChild(r.actor.sprite);

	r.animate();
};

RobotsGame.animate = function(){
	// start the timer for the next animation loop
	requestAnimationFrame(RobotsGame.animate);

	// this is the main render call that makes pixi draw your container and its children.
	RobotsGame.renderer.render(RobotsGame.stage);
};