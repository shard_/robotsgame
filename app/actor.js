var RobotsGame = RobotsGame || {};

RobotsGame.Actor = function(sprite,x,y){

	// Defaults
	if (!x){ x = 0; }
	if (!y){ y = 0; }

	// Sprite
	this.sprite = sprite;

	// Base attributes
	this.health = 50;
	this.stamina = 50;
	this.items = [];

	this.maxHealth = 100;
	this.maxStamina = 100;
	this.maxItems = 5;

	this.x = x;
	this.y = y;
	this.sprite.x = x;
	this.sprite.y = y;

	// Basic Movement API
	this.WalkLeft = function(){
		this.x -= 10;
		this.sprite.x -= 10;
	};

	this.WalkRight = function(){
		this.x += 10;
		this.sprite.x += 10;
	};

	this.Jump = function(){
		this.y ++;
	};

	// Items API
	this.PickupItem = function(item){
		if( this.items.length >= this.maxItems ) { return false; }
		this.items.push(0);
		return true;
	};

	this.DropItem = function(){

	};

	this.UseItem = function(){

	};

};